﻿// Run in split-screen mode

using System.Collections;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.XR;

public class RuntimeCardboardLoader : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(LoadDevice("Cardboard"));
    }

    IEnumerator LoadDevice(string newDevice)
    {
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        XRSettings.enabled = true;
    }
}