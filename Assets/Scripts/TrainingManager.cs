﻿using System;
using System.Collections;
using System.Collections.Generic;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples;
using GoogleVR.HelloVR;
using UnityEngine;

public class TrainingManager : MonoBehaviour
{
    private static int currentStepId = -1, numberOfVideoPlayedInActivity = 0, numberOfErrorVideoPlayerInActivity = 0, stepsFinished = 0;
    private static Step[] steps;

    private static bool generalVideoHasBeenPlayed = false, checkVideoHasBeenPlayed = false;
    public bool endInterviewBool = false;

    private int activitiesCompleted = 0;
    public event Action<string, GameObject, bool> PlayVideo;
    public event Action<string> PlayLastVideo;

    public GameObject nivelDesempeño, problema, tipo, frecuencia, activities, finalEntrevista, demasiadosErrores, creditos;
    public Action activitySelected;

    private static string currentActivity;

    public void Awake()
    {
        steps = new Step[11];

        //Creamos el step de la actividad "COMER Y BEBER"
        steps[0] = new Step("Video13", "Video15", "Video9", "Video6", "COMER Y BEBER",
                            Keywords.correctWordsComerBeber, Keywords.incorrectWordsComerBeber, 5, 1);

        steps[1] = new Step("Video18", "Video20", "Video9", "Video6",
                            "HIGIENE PERSONAL RELACIONADA CON LA MICCION Y DEFECACIÓN", Keywords.correctWordsMiccionDefecacion,
                            Keywords.incorrectWordsMiccionDefecacion, 3, 1);

        steps[2] = new Step("Video22", "Video24", "Video9", "Video6", "LAVARSE", Keywords.correctWordsLavarse,
                            null, 3);

        steps[3] = new Step("Video26", "Video27", "Video9", "Video6",
                            "REALIZAR OTROS CUIDADOS CORPORALES", Keywords.correctWordsCuidadosCorporales, null, 3);

        steps[4] = new Step("Video29", "Video31", "Video9", "Video6", "VESTIRSE", Keywords.correctWordsVestirse,
                            null, 3);

        steps[5] = new Step("Video33", "Video34", "Video9", "Video6",
                            "MANTENIMIENTO DE LA SALUD", Keywords.correctWordsMantenimientoSalud, null, 5);

        steps[6] = new Step("Video36", "Video38", "Video9", "Video6",
                            "CAMBIAR Y MANTENER LA POSICION DEL CUERPO", Keywords.correctWordsPosicionCuerpo, null, 5);

        steps[7] = new Step("Video40", "Video42", "Video9", "Video6", "DESPLAZARSE DENTRO DEL HOGAR",
                            Keywords.correctWordsDentroHogar, null, 5);

        steps[8] = new Step("Video43", "Video43", "Video9", "Video6", "DESPLAZARSE FUERA DEL HOGAR",
                            Keywords.correctWordsFueraHogar, null, 6);

        steps[9] = new Step("Video46", "Video46", "Video9", "Video6", "REALIZAR TAREAS DOMÉSTICAS",
                            Keywords.correctWordsTareasDomesticas, null, 6);

        steps[10] = new Step("Video50", "Video50", "Video9", "Video6", "CIERRE ENTREVISTA",
                            Keywords.correctWordsDespedida, null, 4);




        /*steps[0] = new Step("Video5", "Video6", "Video9", "Video6", "COMER Y BEBER",
                            Keywords.correctWordsComerBeber, Keywords.incorrectWordsComerBeber, 5, 1);

        steps[1] = new Step("Video5", "Video6", "Video9", "Video6",
                            "HIGIENE PERSONAL RELACIONADA CON LA MICCION Y DEFECACIÓN", Keywords.correctWordsMiccionDefecacion,
                            Keywords.incorrectWordsMiccionDefecacion, 3, 1);

        steps[2] = new Step("Video5", "Video6", "Video9", "Video6", "LAVARSE", Keywords.correctWordsLavarse,
                            null, 3);

        steps[3] = new Step("Video5", "Video6", "Video9", "Video6",
                            "REALIZAR OTROS CUIDADOS CORPORALES", Keywords.correctWordsCuidadosCorporales, null, 3);

        steps[4] = new Step("Video5", "Video6", "Video9", "Video6", "VESTIRSE", Keywords.correctWordsVestirse,
                            null, 3);

        steps[5] = new Step("Video5", "Video6", "Video9", "Video6",
                            "MANTENIMIENTO DE LA SALUD", Keywords.correctWordsMantenimientoSalud, null, 5);

        steps[6] = new Step("Video5", "Video6", "Video9", "Video6",
                            "CAMBIAR Y MANTENER LA POSICION DEL CUERPO", Keywords.correctWordsPosicionCuerpo, null, 5);

        steps[7] = new Step("Video5", "Video6", "Video9", "Video6", "DESPLAZARSE DENTRO DEL HOGAR",
                            Keywords.correctWordsDentroHogar, null, 5);

        steps[8] = new Step("Video5", "Video6", "Video9", "Video6", "DESPLAZARSE FUERA DEL HOGAR",
                            Keywords.correctWordsFueraHogar, null, 6);

        steps[9] = new Step("Video5", "Video6", "Video9", "Video6", "REALIZAR TAREAS DOMÉSTICAS",
                            Keywords.correctWordsTareasDomesticas, null, 6);
        steps[10] = new Step("Video50", "Video50", "Video9", "Video6", "CIERRE ENTREVISTA",
                            Keywords.correctWordsDespedida, null, 4);
    */
    }

    public void ExecuteActivity(string actSelected)
    {

        currentActivity = actSelected;
        numberOfErrorVideoPlayerInActivity = 0;
        numberOfVideoPlayedInActivity = 0;

        //Primer paso, hacemos que la apicación grabe lo que dice el usuario:
        FindObjectOfType<Custom_GCSR_Example>().StartRecordButtonOnClickHandler();
    }

    public void EndInterview()
    {
        finalEntrevista.SetActive(false);
        currentActivity = "CIERRE ENTREVISTA";
        FindObjectOfType<Custom_GCSR_Example>().StartRecordButtonOnClickHandler();
    }


    //Este metodo recoge lo que el usuario ha dicho y lo compara
    //con las palabras correctas asociadas al paso actual.
    //Si el usuario tiene más de X aciertos entonces continuará al siguiente paso.
    public void ProcessTranscript(WordInfo[] transcript)
    {
        int numberKeywordsSaidCorrectly = 0;
        int numberKeywordsSaidIncorrectly = 0;
        bool directCheck = false;
        Step stepSelected = null;
        List<string> clearTranscript = new List<string>();

        foreach (Step step in steps)
        {
            if (step.GetStepName().Equals(currentActivity))
            {
                stepSelected = step;
            }
        }


        foreach (WordInfo item in transcript)
        {
            //Primero procesamos la palabra para quitarle cualquier caracter extraño
            if (item.word.Contains("."))
            {
                item.word = item.word.Replace(".", "");
            }
            if (item.word.Contains(","))
            {
                item.word = item.word.Replace(",", String.Empty);
            }

            clearTranscript.Add(item.word);
        }


        foreach (string item in clearTranscript)
        {
            if (stepSelected.GetIncorrectWords() != null)
            {
                //Miramos si la palabra coincide con alguna del diccionario de palabras clave incorrectas
                foreach (string st in stepSelected.GetIncorrectWords())
                {
                    if (item.Equals(st, StringComparison.CurrentCultureIgnoreCase))
                    {
                        numberKeywordsSaidIncorrectly++;
                        Debug.Log("palabra incorrecta: " + st);
                    }
                }
            }

            //Miramos si la palabra coincide con alguna del diccionario de palabras clave correctas
            foreach (string st in stepSelected.GetCorrectWords())
            {
                if (item.Equals(st, StringComparison.CurrentCultureIgnoreCase))
                {
                    numberKeywordsSaidCorrectly++;
                    //Debug.Log("palabra correcta: " + st);
                    if (item.Equals("comprobar") || item.Equals("comprobación"))
                    {
                        directCheck = true;
                    }
                }
            }

            //break;
        }

        Debug.Log("Numero de palabras incorrectas: " + numberKeywordsSaidIncorrectly);
        Debug.Log("Numero de palabras correctas: " + numberKeywordsSaidCorrectly);

        if (endInterviewBool)
        {
            EvaluateEndInterviewTranscript(numberKeywordsSaidCorrectly, numberKeywordsSaidIncorrectly, stepSelected);
        }
        else
        {
            EvaluateTranscript(numberKeywordsSaidCorrectly, numberKeywordsSaidIncorrectly, stepSelected, directCheck);
        }

    }

    public void EvaluateEndInterviewTranscript(int numberKeywordsSaidCorrectly, int numberKeywordsSaidIncorrectly, Step currentStep)
    {
        if (numberKeywordsSaidIncorrectly >= currentStep.GetNIncorrectWords() || numberKeywordsSaidCorrectly < currentStep.GetNCorrectWords())
        {
            if (numberOfErrorVideoPlayerInActivity == 0)
            {
                PlayVideo.Invoke(currentStep.GetAssociatedErrorVideo(), null, false);
                numberOfErrorVideoPlayerInActivity++;
            }
            else
            {
                numberOfErrorVideoPlayerInActivity++;
                //Es el segundo error del usuario por lo que tenemos que escribir
                //en el CSV el apartado que ha escogido y que ha cometido dos errores.
                string[] data = new string[8];
                data[0] = data[1] = data[4] = data[5] = data[6] = data[7] = "";
                data[2] = currentStep.GetStepName();
                data[3] = numberOfErrorVideoPlayerInActivity.ToString();

                CSVManager.AppendToReport(data);
                //MailManager mailManager = FindObjectOfType<MailManager>();
                //mailManager.SendMail();
                TestFilesCreate testFilesCreate = FindObjectOfType<TestFilesCreate>();
                testFilesCreate.Upload();
                //Tenemos que finalizar la aplicación
                PlayVideo.Invoke(currentStep.GetAssociatedErrorVideo2(), null, true);
            }

        }
        if (numberKeywordsSaidCorrectly >= steps[10].GetNCorrectWords())
        {
            PlayLastVideo.Invoke(currentStep.GetAssociatedCheckVideo());
        }
    }
    public void EvaluateTranscript(int numberKeywordsSaidCorrectly, int numberKeywordsSaidIncorrectly, Step currentStep, bool directCheck)
    {

        if (numberKeywordsSaidIncorrectly >= currentStep.GetNIncorrectWords() || numberKeywordsSaidCorrectly < currentStep.GetNCorrectWords())
        {
            if (numberOfErrorVideoPlayerInActivity == 0)
            {
                PlayVideo.Invoke(currentStep.GetAssociatedErrorVideo(), null, false);
                numberOfErrorVideoPlayerInActivity++;
            }
            else
            {
                numberOfErrorVideoPlayerInActivity++;
                //Es el segundo error del usuario por lo que tenemos que escribir
                //en el CSV el apartado que ha escogido y que ha cometido dos errores.
                string[] data = new string[8];
                data[0] = data[1] = data[4] = data[5] = data[6] = data[7] = "";
                data[2] = currentStep.GetStepName();
                data[3] = numberOfErrorVideoPlayerInActivity.ToString();

                CSVManager.AppendToReport(data);
                MailManager mailManager = FindObjectOfType<MailManager>();
                mailManager.SendMail();
                TestFilesCreate testFilesCreate = FindObjectOfType<TestFilesCreate>();
                testFilesCreate.Upload();
                //Tenemos que finalizar la aplicación
                PlayVideo.Invoke(currentStep.GetAssociatedErrorVideo2(), null, true);
            }

        }
        else if (numberKeywordsSaidCorrectly >= currentStep.GetNCorrectWords())
        {

            /*Pasos a seguir:
                -Comprobar si se ha hecho una pregunta de comprobación. En caso afirmativo poner video de comprobación asociado al paso
                -Comprobar si se ha hecho una pregunta normal, en caso afirmativo poner video normal asociado al paso
                -Si se hace una pregunta normal y ya se ha visto el video normal asociado al paso, poner el video de comprobación
                -Si se hace una pregunta de comprobacion y ya se ha visto el video de comprobacion asociado al paso, poner el video normal
                -Si es el segundo video que se ve de este paso entonces el paso estará finalizado, por lo que se procederá a responder las preguntas
                -Una vez respondidas las preguntas tenemos que activar de nuevo el cartel de las actividades
                -Desactivar del cartel de actividades aquella actividad que el usuario ya haya contestado.
            */

            if (directCheck)
            {
                if (numberOfVideoPlayedInActivity == 0)
                {
                    //Reproducimos video comprobacion asociado
                    PlayVideo.Invoke(currentStep.GetAssociatedCheckVideo(), null, false);
                    numberOfVideoPlayedInActivity++;
                    checkVideoHasBeenPlayed = true;
                }
                else if (numberOfVideoPlayedInActivity == 1)
                {
                    if (generalVideoHasBeenPlayed)
                    {
                        PlayVideo.Invoke(currentStep.GetAssociatedCheckVideo(), nivelDesempeño, false);
                        generalVideoHasBeenPlayed = false;
                        checkVideoHasBeenPlayed = false;
                        numberOfVideoPlayedInActivity = 0;
                    }
                    else
                    {
                        //reproducimos video normal
                        PlayVideo.Invoke(currentStep.GetAssociatedGeneralVideo(), nivelDesempeño, false);
                    }

                }
            }
            else
            {
                if (numberOfVideoPlayedInActivity == 0)
                {
                    //reproducimos video normal
                    PlayVideo.Invoke(currentStep.GetAssociatedGeneralVideo(), null, false);
                    numberOfVideoPlayedInActivity++;
                    generalVideoHasBeenPlayed = true;
                }
                else
                {
                    if (checkVideoHasBeenPlayed)
                    {
                        //reproducimos video normal
                        PlayVideo.Invoke(currentStep.GetAssociatedGeneralVideo(), nivelDesempeño, false);
                        generalVideoHasBeenPlayed = false;
                        checkVideoHasBeenPlayed = false;
                        numberOfVideoPlayedInActivity = 0;
                    }
                    else
                    {
                        //reproducimos video comprobación
                        PlayVideo.Invoke(currentStep.GetAssociatedCheckVideo(), nivelDesempeño, false);
                    }
                }
            }
        }
    }


    public void AnswerQuestions(string answer, int position)
    {
        Step stepSelected = null;

        foreach (Step step in steps)
        {
            //Si esto ocurre estoy en el objeto correcto
            if (step.GetStepName().Equals(currentActivity))
            {
                stepSelected = step;
                stepSelected.SetAnswer(answer, position);
            }
        }

        //Si hemos respondido la ultima pregunta escribimos
        //en el CSV y avanzamos al siguiente paso
        if (position == 3)
        {
            string[] data = new string[8];
            data[0] = data[1] = "";
            data[2] = stepSelected.GetStepName();
            data[3] = numberOfErrorVideoPlayerInActivity.ToString();
            for (int i = 4; i < data.Length; i++)
            {
                data[i] = stepSelected.GetAnswers()[i - 4];
            }

            CSVManager.AppendToReport(data);
            NextStep();
        }
    }


    public void NextStep()
    {
        stepsFinished++;

        if (stepsFinished < 3)
        {

            foreach (MeshCollider gm in activities.GetComponentsInChildren<MeshCollider>())
            {
                if (gm.gameObject.name.Equals(currentActivity))
                {
                    gm.enabled = false;
                }
            }
            activities.SetActive(true);
        }
        else
        {
            endInterviewBool = true;
            finalEntrevista.SetActive(true);
        }
    }

    public void EndApplication()
    {
        demasiadosErrores.SetActive(true);
    }
}