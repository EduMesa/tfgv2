﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Step
{
    private string[] correctWords;
    private string[] incorrectWords;
    private int nCorrectWords, nIncorrectWords;
    private string associatedGeneralVideo, associatedCheckVideo, associatedErrorVideo, associatedErrorVideo2;
    private string stepName;

    private string[] answers;

    public Step(string associatedGeneralVideo, string associatedErrorVideo, string associatedErrorVideo2, string[] correctWords, string[] incorrectWords, int nCorrectWords, int nIncorrectWords)
    {
        this.associatedGeneralVideo = associatedGeneralVideo;
        this.associatedErrorVideo = associatedErrorVideo;
        this.associatedErrorVideo2 = associatedErrorVideo2;
        this.correctWords = correctWords;
        this.incorrectWords = incorrectWords;
        this.nCorrectWords = nCorrectWords;
        this.nIncorrectWords = nIncorrectWords;
    }

    public Step(string associatedGeneralVideo, string associatedCheckVideo, string associatedErrorVideo, string associatedErrorVideo2, string stepName, 
                string[] correctWords = null, string[] incorrectWords = null, int nCorrectWords = 0, int nIncorrectWords = 10)
    {
        this.correctWords = correctWords;
        this.incorrectWords = incorrectWords;
        this.associatedGeneralVideo = associatedGeneralVideo;
        this.associatedErrorVideo2 = associatedErrorVideo2;
        this.associatedCheckVideo = associatedCheckVideo;
        this.associatedErrorVideo = associatedErrorVideo;
        this.nCorrectWords = nCorrectWords;
        this.nIncorrectWords = nIncorrectWords;
        this.stepName = stepName;
        answers = new string[4];

    }

    public string[] GetCorrectWords()
    {
        return correctWords;
    }
    public string[] GetIncorrectWords()
    {
        return incorrectWords;
    }

    public int GetNCorrectWords()
    {
        return nCorrectWords;
    }

    public int GetNIncorrectWords()
    {
        return nIncorrectWords;
    }

    /*public ErrorStep GetErrorStep()
    {
        return errorStep;
    }*/

    public string GetAssociatedGeneralVideo()
    {
        return associatedGeneralVideo;
    }
    public string GetAssociatedCheckVideo()
    {
        return associatedCheckVideo;
    }
    public string GetAssociatedErrorVideo()
    {
        return associatedErrorVideo;
    }
    public string GetAssociatedErrorVideo2()
    {
        return associatedErrorVideo2;
    }

    public void SetAnswer(string st, int position)
    {
        answers[position] = st;
    }
    public string[] GetAnswers()
    {
        return answers;
    }
    public string GetStepName()
    {
        return stepName;
    }
}
