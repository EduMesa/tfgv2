﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowItemsRelated : MonoBehaviour
{
    private string activitySelected;
    
    public void SetActivitySelected(string activitySelected)
    {
        this.activitySelected = activitySelected;
    }

    void OnEnable ()
    {
        StartCoroutine(ShowItemsRelatedWithActivity());
    } 

    public IEnumerator ShowItemsRelatedWithActivity()
    {
        gameObject.SetActive(true);
        Transform[] childs = gameObject.GetComponentsInChildren<Transform>(true);

        Debug.Log(activitySelected);

        foreach(Transform tf in childs)
        {
            if(tf.gameObject.name.Equals(activitySelected))
            {
                tf.gameObject.SetActive(true);
            }
        }

        yield return new WaitForSeconds(5f);
        gameObject.SetActive(false);
        FindObjectOfType<TrainingManager>().ExecuteActivity(activitySelected);
    }
}
